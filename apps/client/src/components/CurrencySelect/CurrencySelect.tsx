import { HStack, Heading, Select } from "@chakra-ui/react";
import { Currency, useCurrency } from "../../contexts/currency";

export const CurrencySelect = () => {
  const { setSelected, options } = useCurrency();

  return (
    <HStack>
      <Heading size="sm">CURRENCY</Heading>
      <Select
        size="lg"
        variant="outline"
        w="max-content"
        bgColor="white"
        onChange={(e) => setSelected(e.currentTarget.value as Currency)}
      >
        {options.map((option, i) => {
          return <option key={i}>{option}</option>;
        })}
      </Select>
    </HStack>
  );
};
