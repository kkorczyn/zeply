import { gql } from "@apollo/client";

export const GET_TOP_ADDRESSES = gql`
  query getTop {
    getTopAddresses
  }
`;

export type getTopAddressesResponse = {
  getTopAddresses: string[];
};
