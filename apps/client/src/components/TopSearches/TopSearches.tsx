import { Card, Divider, Heading, ListItem, UnorderedList, VStack } from "@chakra-ui/react";
import { useQuery } from "@apollo/client";

import { GET_TOP_ADDRESSES, getTopAddressesResponse } from "./getTopAddresses.graphql";

export const TopSearches = () => {
  const { loading, error, data } = useQuery<getTopAddressesResponse>(GET_TOP_ADDRESSES, {
    pollInterval: 5000,
    fetchPolicy: "cache-and-network",
  });

  if (!data?.getTopAddresses?.length && loading) return null;

  return (
    <VStack alignItems="start">
      <Heading size="md">TOP SEARCHED ADDRESSES:</Heading>
      {error ? (
        <Card variant="filled" mt={4} p={4} bgColor="red.100">
          {error?.message}
        </Card>
      ) : (
        <UnorderedList ml={10}>
          {data?.getTopAddresses.map((address) => {
            const shortAddress = address.length > 26 ? address.slice(0, 13) + "..." + address.slice(-10) : address;
            return <ListItem key={address}>{shortAddress}</ListItem>;
          })}
        </UnorderedList>
      )}
      <Divider borderBottomWidth={4} mt={4} mb={4} variant="dashed" />
    </VStack>
  );
};
