import { Skeleton, Text } from "@chakra-ui/react";

type EmptyLoadinValueText = {
  loading: boolean;
  value?: string | number;
  suffix?: string;
};

export const EmptyLoadingValueText = ({ loading, value, suffix = "" }: EmptyLoadinValueText) => {
  if (loading) return <Skeleton height="24px" w={40} />;
  if (!value) return <Text fontSize="lg">-</Text>;
  return <Text fontSize="lg">{`${value} ${suffix}`}</Text>;
};
