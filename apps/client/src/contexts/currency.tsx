import { createContext, useCallback, useContext, useEffect, useMemo, useState } from "react";

interface CurrencyContextType {
  selected: Currency;
  convertToSelectedCurrency: (value?: number) => number | undefined | string;
  setSelected: React.Dispatch<React.SetStateAction<Currency>>;
  rates: Record<Currency, number>;
  options: Currency[];
}

type CurrencyProviderProps = {
  children: JSX.Element;
};

export type Currency = "BTC" | "USD" | "EUR";

type ExchangerateResponse = {
  asset_id_base: "BTC";
  rates: { asset_id_quote: Currency; rate: number; time: string }[];
};

const CurrencyContext = createContext<CurrencyContextType | null>(null);

const CurrencyProvider = ({ children }: CurrencyProviderProps) => {
  const [selected, setSelected] = useState<Currency>("BTC");
  const [rates, setRates] = useState<Record<string, number>>({ BTC: 1 });

  const convertToSelectedCurrency = useCallback(
    (value?: number) => {
      if (!value) return;
      if (selected === "BTC") return value;
      return (value * rates[selected]).toFixed(2);
    },
    [rates, selected]
  );

  useEffect(() => {
    const fetcher = async () => {
      const response = await fetch("https://rest.coinapi.io/v1/exchangerate/BTC?filter_asset_id=USD,EUR", {
        headers: { "X-CoinAPI-Key": import.meta.env.VITE_COINAPI_API_KEY },
      });
      const data = (await response.json()) as ExchangerateResponse;

      const rates: Record<string, number> = { BTC: 1 };
      data.rates.forEach(({ asset_id_quote, rate }) => {
        rates[asset_id_quote] = rate;
      });

      setRates(rates);
    };

    fetcher();
    const t = setInterval(fetcher, 1000 * 60);
    return () => {
      clearInterval(t);
    };
  }, []);

  const options = useMemo(() => Object.keys(rates), [rates]) as Currency[];

  return (
    <CurrencyContext.Provider value={{ selected, setSelected, rates, options, convertToSelectedCurrency }}>
      {children}
    </CurrencyContext.Provider>
  );
};

const useCurrency = () => {
  const currencyContext = useContext(CurrencyContext);

  if (!currencyContext) {
    throw new Error("useCurrency has to be used within <CurrencyProvider>");
  }

  return currencyContext;
};

export { useCurrency, CurrencyProvider };
