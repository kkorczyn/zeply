import { useToast } from "@chakra-ui/react";
import { createContext, useCallback, useContext, useEffect, useRef, useState } from "react";

interface SubscribedHashesContextType {
  subscribe: (hash: string) => void;
  subscriptionList: Record<string, string[]>;
}

type SubscribedHashesProviderProps = {
  children: JSX.Element;
};

const SubscribedHashesContext = createContext<SubscribedHashesContextType | null>(null);

const SubscribedHashesProvider = ({ children }: SubscribedHashesProviderProps) => {
  const [subscriptionList, setSubscriptionList] = useState<Record<string, string[]>>({});
  const socket = useRef<WebSocket>();
  const toast = useToast();

  useEffect(() => {
    if (!socket.current || socket.current.CLOSED || socket.current.CLOSING) {
      socket.current = new WebSocket(
        `wss://socket.blockcypher.com/v1/btc/main?token=${import.meta.env.VITE_BLOCKCYPHER_TOKEN}`
      );

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      socket.current.onmessage = ({ data }: any) => {
        const parsedData = JSON.parse(data);
        toast({
          title: `New event on hash ${parsedData.hash}.`,
          status: "info",
          isClosable: true,
        });

        setSubscriptionList((prev) => ({
          ...prev,
          [parsedData.hash]: [data, ...(prev[parsedData.hash] || [])],
        }));
      };
    }

    return () => {
      socket.current?.close();
    };
  }, [toast]);

  const subscribe = useCallback(
    (hash: string) => {
      if (subscriptionList[hash]) return;
      setSubscriptionList((prev) => ({ ...prev, [hash]: [] }));
      socket.current?.send(JSON.stringify({ hash }));
    },
    [subscriptionList]
  );

  return (
    <SubscribedHashesContext.Provider value={{ subscribe, subscriptionList }}>
      {children}
    </SubscribedHashesContext.Provider>
  );
};

const useSubscribedHashes = () => {
  const subscribedHashesContext = useContext(SubscribedHashesContext);

  if (!subscribedHashesContext) {
    throw new Error("useSubscribedHashes has to be used within <SubscribedHashesProvider>");
  }

  return subscribedHashesContext;
};

export { useSubscribedHashes, SubscribedHashesProvider };
