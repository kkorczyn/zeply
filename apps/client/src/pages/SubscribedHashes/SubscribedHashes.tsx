import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Box,
  Button,
  Card,
  CardBody,
  CardHeader,
  Flex,
  Heading,
  Input,
  useMediaQuery,
} from "@chakra-ui/react";
import { useMemo, useState } from "react";

import { useSubscribedHashes } from "../../contexts/subscribeToHash";

type SubscriptionDetails = { name: string; events: string[] };

export const SubscribedHashes = () => {
  const [isLargerThan768] = useMediaQuery("(min-width: 768px)");
  const [searchText, setSearchText] = useState("");
  const { subscribe, subscriptionList } = useSubscribedHashes();

  const hashesList = useMemo(() => {
    const list: SubscriptionDetails[] = [];
    for (const key in subscriptionList) {
      if (Object.prototype.hasOwnProperty.call(subscriptionList, key)) {
        const value = subscriptionList[key];
        list.push({ name: key, events: value });
      }
    }
    return list;
  }, [subscriptionList]);

  return (
    <>
      <Flex flexDirection={isLargerThan768 ? "row" : "column"}>
        <Input
          placeholder="Subscribe to hash"
          size="lg"
          width="auto"
          flexGrow={1}
          mr={isLargerThan768 ? 2 : undefined}
          mb={isLargerThan768 ? undefined : 2}
          onChange={(event) => setSearchText(event.currentTarget.value)}
          value={searchText}
        />
        <Button colorScheme="blue" size="lg" onClick={() => subscribe(searchText)}>
          Subscribe
        </Button>
      </Flex>

      <Card variant="filled" mt={4} pb={4}>
        <CardHeader>
          <Heading size="lg">SUBSCRIBED HASHES</Heading>
        </CardHeader>

        <CardBody overflow="auto" pb={2}>
          <Accordion allowToggle>
            {hashesList.map(({ name, events }) => {
              return (
                <AccordionItem key={name}>
                  <AccordionButton>
                    <Box as="span" flex="1" textAlign="left">
                      {name}
                    </Box>
                    <AccordionIcon />
                  </AccordionButton>
                  <AccordionPanel pb={4}>
                    {events.length ? (
                      events.map((event) => (
                        <Card p={2}>
                          <pre>{event}</pre>
                        </Card>
                      ))
                    ) : (
                      <Card p={2}>No events captured for this hash.</Card>
                    )}
                  </AccordionPanel>
                </AccordionItem>
              );
            })}
          </Accordion>
        </CardBody>
      </Card>
    </>
  );
};
