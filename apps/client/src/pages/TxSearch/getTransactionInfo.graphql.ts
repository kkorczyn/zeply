import { gql } from "@apollo/client";

export const GET_TX_INFO = gql`
  query getTransactionInfo($txId: String!) {
    getTransactionInfo(txId: $txId) {
      fees
      hasBTCInput
      hash
      numberOfConfirmations
      receivedAt
      size
      status
      totalBTCOutput
      totalBTCInput
    }
  }
`;

export type getTransactionInfoResponse = {
  getTransactionInfo: {
    fees: number;
    hasBTCInput: boolean;
    hash: string;
    numberOfConfirmations: number;
    receivedAt: string;
    size: number;
    status: "confirmed" | "pending";
    totalBTCOutput: number;
    totalBTCInput: number;
  };
};
