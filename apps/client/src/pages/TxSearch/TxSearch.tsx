import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Flex,
  Heading,
  Input,
  InputGroup,
  InputLeftElement,
  useMediaQuery,
} from "@chakra-ui/react";
import { Search2Icon } from "@chakra-ui/icons";
import { GET_TX_INFO, getTransactionInfoResponse } from "./getTransactionInfo.graphql";
import { useLazyQuery } from "@apollo/client";
import React from "react";
import { EmptyLoadingValueText } from "../../components/EmptyLoadingValueText/EmptyLoadingValueText";
import { useCurrency } from "../../contexts/currency";

export const TxSearch = () => {
  const [searchText, setSearchText] = React.useState("");
  const [getTransactionInfo, { loading, error, data }] = useLazyQuery<getTransactionInfoResponse>(GET_TX_INFO);
  const { convertToSelectedCurrency, selected: selectedCurrency } = useCurrency();
  const [isLargerThan768] = useMediaQuery("(min-width: 768px)");

  return (
    <>
      <Flex flexDirection={isLargerThan768 ? "row" : "column"}>
        <InputGroup>
          <InputLeftElement pointerEvents="none" w={12} h={12}>
            <Search2Icon color="gray.500" boxSize={4} />
          </InputLeftElement>
          <Input
            placeholder="Search for transactions"
            size="lg"
            width="auto"
            flexGrow={1}
            mr={isLargerThan768 ? 2 : undefined}
            mb={isLargerThan768 ? undefined : 2}
            pl={12}
            onChange={(event) => setSearchText(event.currentTarget.value)}
            value={searchText}
          />
        </InputGroup>
        <Button colorScheme="blue" size="lg" onClick={() => getTransactionInfo({ variables: { txId: searchText } })}>
          Search
        </Button>
      </Flex>

      {error ? (
        <Card variant="filled" mt={4} p={4} bgColor="red.100">
          {error?.message}
        </Card>
      ) : (
        <Card variant="filled" mt={4}>
          <CardHeader>
            <Heading size="lg">TRANSACTION</Heading>
            <EmptyLoadingValueText loading={loading} value={data?.getTransactionInfo?.hash} />
          </CardHeader>

          <CardBody>
            <Card variant="outline" p={4} mb={1}>
              <Flex justifyContent="space-between" alignItems="center">
                <Heading size="md">Received time</Heading>
                <EmptyLoadingValueText
                  loading={loading}
                  value={
                    data?.getTransactionInfo?.receivedAt
                      ? new Date(data?.getTransactionInfo?.receivedAt).toLocaleString()
                      : undefined
                  }
                />
              </Flex>
            </Card>
            <Card variant="outline" p={4} mb={1}>
              <Flex justifyContent="space-between" alignItems="center">
                <Heading size="md">Status</Heading>
                <EmptyLoadingValueText loading={loading} value={data?.getTransactionInfo?.status} />
              </Flex>
            </Card>
            <Card variant="outline" p={4} mb={1}>
              <Flex justifyContent="space-between" alignItems="center">
                <Heading size="md">Size</Heading>
                <EmptyLoadingValueText loading={loading} suffix="B" value={data?.getTransactionInfo?.size} />
              </Flex>
            </Card>
            <Card variant="outline" p={4} mb={1}>
              <Flex justifyContent="space-between" alignItems="center">
                <Heading size="md">Number of confiramtions</Heading>
                <EmptyLoadingValueText loading={loading} value={data?.getTransactionInfo?.numberOfConfirmations} />
              </Flex>
            </Card>
            <Card variant="outline" p={4} mb={1}>
              <Flex justifyContent="space-between" alignItems="center">
                <Heading size="md">Total BTC input</Heading>
                <EmptyLoadingValueText
                  loading={loading}
                  suffix={selectedCurrency}
                  value={convertToSelectedCurrency(data?.getTransactionInfo?.totalBTCInput)}
                />
              </Flex>
            </Card>
            <Card variant="outline" p={4} mb={1}>
              <Flex justifyContent="space-between" alignItems="center">
                <Heading size="md">Total BTC output</Heading>
                <EmptyLoadingValueText
                  loading={loading}
                  suffix={selectedCurrency}
                  value={convertToSelectedCurrency(data?.getTransactionInfo?.totalBTCOutput)}
                />
              </Flex>
            </Card>
            <Card variant="outline" p={4} mb={1}>
              <Flex justifyContent="space-between" alignItems="center">
                <Heading size="md">Total fees</Heading>
                <EmptyLoadingValueText
                  loading={loading}
                  suffix={selectedCurrency}
                  value={convertToSelectedCurrency(data?.getTransactionInfo?.fees)}
                />
              </Flex>
            </Card>
          </CardBody>
        </Card>
      )}
    </>
  );
};
