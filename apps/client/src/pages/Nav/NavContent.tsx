import { Divider, Heading } from "@chakra-ui/react";
import { NavLink } from "react-router-dom";

import { TopSearches } from "../../components/TopSearches/TopSearches";
import { CurrencySelect } from "../../components/CurrencySelect/CurrencySelect";

type NavContentProps = {
  onLinkClick?: () => void;
};

export const NavContent = ({ onLinkClick }: NavContentProps) => {
  return (
    <>
      <Heading>
        <NavLink to="/address-search" onClick={() => onLinkClick?.()}>
          Address search
        </NavLink>
      </Heading>
      <Heading>
        <NavLink to="/tx-search" onClick={() => onLinkClick?.()}>
          Transaction search
        </NavLink>
      </Heading>
      <Heading>
        <NavLink to="/subscribe-to-hash" onClick={() => onLinkClick?.()}>
          Subscribe to hash
        </NavLink>
      </Heading>
      <Divider borderBottomWidth={4} mt={4} mb={4} variant="dashed" />
      <TopSearches />
      <CurrencySelect />
    </>
  );
};
