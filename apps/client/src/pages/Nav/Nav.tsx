import {
  Container,
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerOverlay,
  Flex,
  HStack,
  IconButton,
  useDisclosure,
  useMediaQuery,
} from "@chakra-ui/react";

import { Outlet } from "react-router-dom";
import { NavContent } from "./NavContent";
import { HamburgerIcon } from "@chakra-ui/icons";
import { useMemo } from "react";

export const Nav = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [isLargerThan768] = useMediaQuery("(min-width: 768px)");
  const [isLargerThan1024] = useMediaQuery("(min-width: 1024px)");
  const drawerProps = useMemo(() => {
    if (!isLargerThan768) {
      return { placement: "top", size: "full" } as const;
    } else if (isLargerThan768 && !isLargerThan1024) {
      return { placement: "right", size: "lg" } as const;
    }
  }, [isLargerThan1024, isLargerThan768]);

  if (isLargerThan1024) {
    return (
      <Flex h="100vh" alignItems="stretch">
        <Container bgColor="gray.300" m={0} maxW={450} pt={10}>
          <NavContent />
        </Container>
        <Container p={4} maxW={800} m={0}>
          <Outlet />
        </Container>
      </Flex>
    );
  }

  return (
    <>
      <HStack p={4}>
        <IconButton aria-label="test" icon={<HamburgerIcon />} onClick={onOpen} ml="auto" variant="ghost" />
      </HStack>
      <Drawer isOpen={isOpen} onClose={onClose} {...drawerProps}>
        <DrawerOverlay />
        <DrawerContent>
          <DrawerCloseButton />

          <DrawerBody>
            <NavContent onLinkClick={onClose} />
          </DrawerBody>
        </DrawerContent>
      </Drawer>
      <Container p={4} maxW={800}>
        <Outlet />
      </Container>
    </>
  );
};
