import { gql } from "@apollo/client";

export const GET_ADDRESS_INFO = gql`
  query getAddressInfo($address: String!) {
    getAddressInfo(address: $address) {
      address
      numberOfConfirmedTx
      totalReceived
      totalSpent
      totalUnspent
      balance
    }
  }
`;

export type getGetAddressInfo = {
  getAddressInfo: {
    address: string;
    numberOfConfirmedTx: number;
    totalReceived: number;
    totalSpent: number;
    totalUnspent: number;
    balance: number;
  };
};
