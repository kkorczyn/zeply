import React from "react";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Flex,
  Heading,
  Input,
  InputGroup,
  InputLeftElement,
  useMediaQuery,
} from "@chakra-ui/react";
import { Search2Icon } from "@chakra-ui/icons";

import { GET_ADDRESS_INFO, getGetAddressInfo } from "./getAddressInfo.graphql";
import { useLazyQuery } from "@apollo/client";
import { EmptyLoadingValueText } from "../../components/EmptyLoadingValueText/EmptyLoadingValueText";
import { useCurrency } from "../../contexts/currency";

export const AddressSearch = () => {
  const [searchText, setSearchText] = React.useState("");
  const [getAddressInfo, { loading, error, data }] = useLazyQuery<getGetAddressInfo>(GET_ADDRESS_INFO);
  const { convertToSelectedCurrency, selected: selectedCurrency } = useCurrency();
  const [isLargerThan768] = useMediaQuery("(min-width: 768px)");

  return (
    <>
      <Flex flexDirection={isLargerThan768 ? "row" : "column"}>
        <InputGroup>
          <InputLeftElement pointerEvents="none" w={12} h={12}>
            <Search2Icon color="gray.500" boxSize={4} />
          </InputLeftElement>
          <Input
            placeholder="Search for address"
            size="lg"
            width="auto"
            flexGrow={1}
            mr={isLargerThan768 ? 2 : undefined}
            mb={isLargerThan768 ? undefined : 2}
            pl={12}
            value={searchText}
            onChange={(event) => setSearchText(event.currentTarget.value)}
          />
        </InputGroup>
        <Button colorScheme="blue" size="lg" onClick={() => getAddressInfo({ variables: { address: searchText } })}>
          Search
        </Button>
      </Flex>

      {error ? (
        <Card variant="filled" mt={4} p={4} bgColor="red.100">
          {error?.message}
        </Card>
      ) : (
        <Card variant="filled" mt={4}>
          <CardHeader>
            <Heading size="lg">ADDRESS</Heading>
            <Heading size="sm">
              <EmptyLoadingValueText loading={loading} value={data?.getAddressInfo?.address} />
            </Heading>
          </CardHeader>

          <CardBody>
            <Card variant="outline" p={4} mb={1}>
              <Flex justifyContent="space-between" alignItems="center">
                <Heading size="md">Number of confirmed transactions</Heading>
                <EmptyLoadingValueText loading={loading} value={data?.getAddressInfo?.numberOfConfirmedTx} />
              </Flex>
            </Card>
            <Card variant="outline" p={4} mb={1}>
              <Flex justifyContent="space-between" alignItems="center">
                <Heading size="md">Total BTC received</Heading>
                <EmptyLoadingValueText
                  loading={loading}
                  value={convertToSelectedCurrency(data?.getAddressInfo?.totalReceived)}
                  suffix={selectedCurrency}
                />
              </Flex>
            </Card>
            <Card variant="outline" p={4} mb={1}>
              <Flex justifyContent="space-between" alignItems="center">
                <Heading size="md">Total BTC spent</Heading>
                <EmptyLoadingValueText
                  loading={loading}
                  value={convertToSelectedCurrency(data?.getAddressInfo?.totalSpent)}
                  suffix={selectedCurrency}
                />
              </Flex>
            </Card>
            <Card variant="outline" p={4} mb={1}>
              <Flex justifyContent="space-between" alignItems="center">
                <Heading size="md">Total BTC unspent</Heading>
                <EmptyLoadingValueText
                  loading={loading}
                  value={convertToSelectedCurrency(data?.getAddressInfo?.totalUnspent)}
                  suffix={selectedCurrency}
                />
              </Flex>
            </Card>
            <Card variant="outline" p={4} mb={1}>
              <Flex justifyContent="space-between" alignItems="center">
                <Heading size="md">Current balance</Heading>
                <EmptyLoadingValueText
                  loading={loading}
                  value={convertToSelectedCurrency(data?.getAddressInfo?.balance)}
                  suffix={selectedCurrency}
                />
              </Flex>
            </Card>
          </CardBody>
        </Card>
      )}
    </>
  );
};
