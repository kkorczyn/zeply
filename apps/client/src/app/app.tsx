import { ChakraProvider } from "@chakra-ui/react";
import { extendTheme } from "@chakra-ui/react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { ApolloClient, InMemoryCache, ApolloProvider } from "@apollo/client";

import { AddressSearch } from "../pages/AddressSearch/AddressSearch";
import { TxSearch } from "../pages/TxSearch/TxSearch";
import { SubscribedHashesProvider } from "../contexts/subscribeToHash";
import { SubscribedHashes } from "../pages/SubscribedHashes/SubscribedHashes";
import { Nav } from "../pages/Nav/Nav";

import "@fontsource-variable/source-code-pro";
import { CurrencyProvider } from "../contexts/currency";

const client = new ApolloClient({
  uri: import.meta.env.VITE_GRAPHQL_ENDPOINT,
  cache: new InMemoryCache(),
});

const theme = extendTheme({
  fonts: {
    heading: `'Source Code Pro Variable', sans-serif`,
    body: `'Source Code Pro Variable', sans-serif`,
  },
});

export function App() {
  return (
    <ApolloProvider client={client}>
      <ChakraProvider theme={theme}>
        <CurrencyProvider>
          <SubscribedHashesProvider>
            <BrowserRouter>
              <Routes>
                <Route path="/" element={<Nav />}>
                  <Route path="/address-search" element={<AddressSearch />} />
                  <Route path="/tx-search" element={<TxSearch />} />
                  <Route path="/subscribe-to-hash" element={<SubscribedHashes />} />
                </Route>
              </Routes>
            </BrowserRouter>
          </SubscribedHashesProvider>
        </CurrencyProvider>
      </ChakraProvider>
    </ApolloProvider>
  );
}

export default App;
