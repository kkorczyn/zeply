/* eslint-disable @typescript-eslint/no-unused-vars */
import { Test, TestingModule } from "@nestjs/testing";
import { BitcoinResolver } from "./bitcoin.resolver";
import { BitcoinService } from "./bitcoin.service";
import { txInfoExpectedResult } from "./__mocks__/txResponse.mock";
import { addressInfoExpectedResult } from "./__mocks__/addressResponse.mock";
import { AddressInfo } from "./models/address-info.model";
import { TxInfo } from "./models/tx-info.model";

const bitcoinServiceMock = {
  getAddressInfo: jest.fn((_address: string): AddressInfo => addressInfoExpectedResult),
  getTransactionInfo: jest.fn((_id: string): TxInfo => txInfoExpectedResult),
  getTopAddresses: () => ["1", "2", "3", "4", "5"],
};
describe("BitcoinResolver", () => {
  let resolver: BitcoinResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BitcoinResolver, { provide: BitcoinService, useValue: bitcoinServiceMock }],
    }).compile();

    resolver = module.get<BitcoinResolver>(BitcoinResolver);
  });

  it("should be defined", () => {
    expect(resolver).toBeDefined();
  });

  it("should return address information", async () => {
    const result = resolver.getAddressInfo("sample-address");
    expect(result).toEqual(addressInfoExpectedResult);
  });

  it("should return transaction information", async () => {
    const result = resolver.getTransactionInfo("sample-tx-id");
    expect(result).toEqual(txInfoExpectedResult);
  });

  it("should return top addresses", async () => {
    const result = resolver.getTopAddresses();
    expect(result).toEqual(["1", "2", "3", "4", "5"]);
  });
});
