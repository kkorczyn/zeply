import { AddressResponse } from "../blockcypher.interface";
import { AddressInfo } from "../models/address-info.model";

export const addressResponseMock: AddressResponse = {
  address: "1ABCDEF123456789",
  total_received: 1_000_000_000,
  total_sent: 400_000_000,
  balance: 250_000_000,
  unconfirmed_balance: 200,
  final_balance: 700,
  n_tx: 10,
  unconfirmed_n_tx: 2,
  final_n_tx: 12,
  txs: [],
  txrefs: [],
};

export const addressInfoExpectedResult: AddressInfo = {
  address: "1ABCDEF123456789",
  balance: 2.5,
  numberOfConfirmedTx: 10,
  totalReceived: 10,
  totalSpent: 4,
  totalUnspent: 6,
};
