import { TXInput, TXOutput, TxResponse } from "../blockcypher.interface";
import { TxInfo } from "../models/tx-info.model";

export const txResponseMock: TxResponse = {
  block_height: 1000,
  hash: "TXHASH123456789",
  addresses: ["1ABCDEF123456789", "1XYZABC987654321"],
  total: 1000,
  fees: 1020,
  size: 200,
  vsize: 180,
  preference: "high",
  relayed_by: "peer1.example.com",
  received: "2023-06-07T12:34:56Z",
  ver: 1,
  lock_time: 0,
  double_spend: false,
  vin_sz: 2,
  vout_sz: 1,
  confirmations: 6,
  inputs: [],
  outputs: [],
};

const input1: TXInput = {
  prev_hash: "PREVHASH123",
  output_index: 0,
  output_value: 500_000_000,
  script_type: "scripType1",
  script: "script1",
  addresses: ["1ABCDEF123456789"],
};

const input2: TXInput = {
  prev_hash: "PREVHASH456",
  output_index: 1,
  output_value: 300_000_000,
  script_type: "scripType2",
  script: "script2",
  addresses: ["1XYZABC987654321"],
};

const output1: TXOutput = {
  value: 700_000_000,
  script: "outputScript1",
  addresses: ["1DEFABC987654321"],
  script_type: "scripType3",
};

txResponseMock.inputs.push(input1, input2);
txResponseMock.outputs.push(output1);

export const txInfoExpectedResult: TxInfo = {
  fees: 0.0000102,
  hasBTCInput: true,
  hash: txResponseMock.hash,
  numberOfConfirmations: txResponseMock.confirmations,
  receivedAt: txResponseMock.confirmed,
  size: txResponseMock.size,
  status: "confirmed",
  totalBTCOutput: 7,
  totalBTCInput: 8,
};
