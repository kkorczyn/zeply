import { AddressResponse, TxResponse } from "./blockcypher.interface";
import { AddressInfo } from "./models/address-info.model";
import { TxInfo } from "./models/tx-info.model";

const convertSatoToBtc = (amount: number): number => amount / 100_000_000;

export const serializeAddressResponse = (input: AddressResponse): AddressInfo => {
  return {
    address: input.address,
    balance: convertSatoToBtc(input.balance),
    numberOfConfirmedTx: input.n_tx,
    totalReceived: convertSatoToBtc(input.total_received),
    totalSpent: convertSatoToBtc(input.total_sent),
    totalUnspent: convertSatoToBtc(input.total_received - input.total_sent),
  };
};

export const serializeTxResponse = (tx: TxResponse): TxInfo => {
  // NOTE: -1 represents coinbase transactions. Coinbase tx miss value
  const totalSatoInput = tx.inputs.reduce((prev, cur) => {
    if (!cur.output_value || prev === -1) {
      return -1;
    } else {
      return (prev += cur.output_value);
    }
  }, 0);

  const totalSatoOutput = tx.outputs.reduce((prev, cur) => prev + cur.value, 0);

  return {
    hash: tx.hash,
    fees: convertSatoToBtc(tx.fees),
    totalBTCInput: totalSatoInput >= 0 ? convertSatoToBtc(totalSatoInput) : undefined,
    hasBTCInput: totalSatoInput >= 0,
    totalBTCOutput: convertSatoToBtc(totalSatoOutput),
    receivedAt: tx.confirmed,
    numberOfConfirmations: tx.confirmations,
    size: tx.size,
    status: tx.confirmations >= 3 ? "confirmed" : "pending",
  };
};
