import { ObjectType, Field, Int, Float } from "@nestjs/graphql";

const txStatus = {
  confirmed: "confirmed",
  pending: "pending",
} as const;

type TxStatus = (typeof txStatus)[keyof typeof txStatus];

@ObjectType()
export class TxInfo {
  @Field()
  hash: string;

  @Field()
  receivedAt: string;

  @Field()
  status: TxStatus;

  @Field(() => Int)
  size: number;

  @Field(() => Int)
  numberOfConfirmations: number;

  @Field(() => Float)
  totalBTCInput?: number;

  @Field(() => Boolean)
  hasBTCInput: boolean;

  @Field(() => Float)
  totalBTCOutput: number;

  @Field(() => Float)
  fees: number;
}
