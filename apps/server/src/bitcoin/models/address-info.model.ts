import { ObjectType, Field, Float, Int } from "@nestjs/graphql";

@ObjectType()
export class AddressInfo {
  @Field(() => String)
  address: string;

  @Field(() => Int)
  numberOfConfirmedTx: number;

  @Field(() => Float)
  totalReceived: number;

  @Field(() => Float)
  totalSpent: number;

  @Field(() => Float)
  totalUnspent: number;

  @Field(() => Float)
  balance: number;
}
