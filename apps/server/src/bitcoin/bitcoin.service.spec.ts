import { Test, TestingModule } from "@nestjs/testing";
import { BitcoinService } from "./bitcoin.service";
import { AddressEntity } from "./entities/address.entity";
import { TypeOrmModule } from "@nestjs/typeorm";
import axios from "axios";
import { addressInfoExpectedResult, addressResponseMock } from "./__mocks__/addressResponse.mock";
import { txInfoExpectedResult, txResponseMock } from "./__mocks__/txResponse.mock";

describe("BitcoinService", () => {
  let service: BitcoinService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          type: "sqlite",
          database: ":memory:",
          entities: [AddressEntity],
          synchronize: true,
        }),
        TypeOrmModule.forFeature([AddressEntity]),
      ],
      providers: [BitcoinService],
    }).compile();

    service = module.get<BitcoinService>(BitcoinService);
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });

  describe("getAddressInfo", () => {
    it("should return address information", async () => {
      const mockResponse = { data: addressResponseMock };
      jest.spyOn(axios, "get").mockResolvedValue(mockResponse);

      const address = "sample-address";
      const addressInfo = await service.getAddressInfo(address);
      expect(addressInfo).toEqual(addressInfoExpectedResult);

      expect(axios.get).toHaveBeenCalledWith(`${service["baseUrl"]}/addrs/${address}`);
    });

    it("should throw an error when address information retrieval fails", async () => {
      jest.spyOn(axios, "get").mockRejectedValue(new Error("Failed to retrieve address information"));

      await expect(service.getAddressInfo("sample-address")).rejects.toThrowError(
        "Failed to retrieve address information"
      );
    });
  });

  describe("getTxInfo", () => {
    it("should return transaction information", async () => {
      const mockResponse = { data: txResponseMock };
      jest.spyOn(axios, "get").mockResolvedValue(mockResponse);

      const txId = "sample-tx-id";
      const txInfo = await service.getTransactionInfo(txId);
      expect(txInfo).toEqual(txInfoExpectedResult);

      expect(axios.get).toHaveBeenCalledWith(`${service["baseUrl"]}/txs/${txId}`);
    });

    it("should throw an error when transaction information retrieval fails", async () => {
      jest.spyOn(axios, "get").mockRejectedValue(new Error("Failed to retrieve transaction information"));

      await expect(service.getTransactionInfo("sample-tx-id")).rejects.toThrowError(
        "Failed to retrieve transaction information"
      );
    });
  });

  describe("incrementAddressSearch", () => {
    it("should increment the address search count for a new address", async () => {
      const address = "sample-address";
      const notInDb = await service["addressRepo"].findOne({ where: { address } });
      expect(notInDb).toEqual(null);

      const result = await service.incrementAddressSearch(address);

      expect(result).toMatchObject({ count: 1, address: "sample-address" });
    });

    it("should increment the address search count for an existing address", async () => {
      const address = "sample-address";
      await service.incrementAddressSearch(address);
      const inDb = await service["addressRepo"].findOne({ where: { address } });
      expect(inDb).not.toEqual(null);

      const result = await service.incrementAddressSearch(address);

      expect(result).toMatchObject({ count: 2, address: "sample-address" });
    });

    it("should log an error when incrementing address search fails", async () => {
      jest.spyOn(service["addressRepo"], "findOne").mockRejectedValue(new Error("Failed to increment address search"));

      const address = "sample-address";
      const consoleSpy = jest.spyOn(console, "log");
      await service.incrementAddressSearch(address);

      expect(consoleSpy).toHaveBeenCalledWith("Failed to increment address searches.");
    });
  });

  describe("getTopAddresses", () => {
    it("should return top addresses", async () => {
      const address1 = "sample-address-1";
      await service.incrementAddressSearch(address1);

      const address2 = "sample-address-2";
      await service.incrementAddressSearch(address2);
      await service.incrementAddressSearch(address2);

      const address3 = "sample-address-3";
      await service.incrementAddressSearch(address3);
      await service.incrementAddressSearch(address3);
      await service.incrementAddressSearch(address3);

      const address4 = "sample-address-4";
      await service.incrementAddressSearch(address4);
      await service.incrementAddressSearch(address4);
      await service.incrementAddressSearch(address4);
      await service.incrementAddressSearch(address4);

      const address5 = "sample-address-5";
      await service.incrementAddressSearch(address5);
      await service.incrementAddressSearch(address5);
      await service.incrementAddressSearch(address5);
      await service.incrementAddressSearch(address5);
      await service.incrementAddressSearch(address5);

      const address6 = "sample-address-6";
      await service.incrementAddressSearch(address6);
      await service.incrementAddressSearch(address6);
      await service.incrementAddressSearch(address6);
      await service.incrementAddressSearch(address6);
      await service.incrementAddressSearch(address6);
      await service.incrementAddressSearch(address6);

      const expectedResult = [address6, address5, address4, address3, address2];
      const result = await service.getTopAddresses();

      expect(result).toEqual(expectedResult);
    });
  });
});

const times = async (n: number, fn: () => unknown) => {
  for (let i = 0; i < n; i++) {
    fn();
  }
};
