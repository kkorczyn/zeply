import { Resolver, Query, Args } from "@nestjs/graphql";
import { BitcoinService } from "./bitcoin.service";
import { AddressInfo } from "./models/address-info.model";
import { TxInfo } from "./models/tx-info.model";

@Resolver(() => AddressInfo)
export class BitcoinResolver {
  constructor(private readonly bitcoinService: BitcoinService) {}

  @Query(() => AddressInfo)
  getAddressInfo(@Args("address") address: string) {
    return this.bitcoinService.getAddressInfo(address);
  }

  @Query(() => TxInfo)
  getTransactionInfo(@Args("txId") txId: string) {
    return this.bitcoinService.getTransactionInfo(txId);
  }

  @Query(() => [String])
  getTopAddresses() {
    return this.bitcoinService.getTopAddresses();
  }
}
