import { Module } from "@nestjs/common";
import { BitcoinService } from "./bitcoin.service";
import { BitcoinResolver } from "./bitcoin.resolver";
import { AddressEntity } from "./entities/address.entity";
import { TypeOrmModule } from "@nestjs/typeorm";

@Module({
  imports: [TypeOrmModule.forFeature([AddressEntity])],
  providers: [BitcoinResolver, BitcoinService],
})
export class BitcoinModule {}
