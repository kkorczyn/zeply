import { AddressResponse, TxResponse } from "./blockcypher.interface";
import { serializeAddressResponse, serializeTxResponse } from "./blockcypher.util";
import { AddressInfo } from "./models/address-info.model";
import { TxInfo } from "./models/tx-info.model";

describe("Blockcypher utils", () => {
  test("serializeAddressResponse should return correct object", () => {
    const input = {
      balance: 123_456_789,
      total_received: 555_555_555,
      total_sent: 444_444_444,
      n_tx: 100,
    } as AddressResponse;
    const expectedResult: AddressInfo = {
      balance: 1.23456789,
      numberOfConfirmedTx: 100,
      totalReceived: 5.55555555,
      totalSpent: 4.44444444,
      totalUnspent: 1.11111111,
    };

    const result = serializeAddressResponse(input);
    expect(result).toEqual(expectedResult);
  });

  describe("serializeTxResponse", () => {
    const input: TxResponse = {
      hash: "1234",
      fees: 123_456_789,
      confirmed: "2023-06-07T13:07:30.362Z",
      confirmations: 100,
      size: 305,
      inputs: [{ output_value: 111_111_111 }, { output_value: 222_222_222 }],
      outputs: [{ value: 111_111_111 }, { value: 333_333_333 }],
    } as TxResponse;

    const expectedResult: TxInfo = {
      hash: input.hash,
      fees: 1.23456789,
      totalBTCInput: 3.33_333_333,
      hasBTCInput: true,
      totalBTCOutput: 4.44_444_444,
      receivedAt: "2023-06-07T13:07:30.362Z",
      numberOfConfirmations: input.confirmations,
      size: input.size,
      status: "confirmed",
    };

    test("should return correct object [NO coinbase input]", () => {
      const result = serializeTxResponse(input);
      expect(result).toEqual(expectedResult);
    });

    test("should return correct object [coinbase input]", () => {
      const result = serializeTxResponse({
        ...input,
        inputs: [...input.inputs, { output_value: undefined }],
      } as TxResponse);
      expect(result).toEqual({ ...expectedResult, hasBTCInput: false, totalBTCInput: undefined });
    });

    test("should return correct object [NO confirmations]", () => {
      const result = serializeTxResponse({ ...input, confirmations: 2 });
      expect(result).toEqual({ ...expectedResult, status: "pending", numberOfConfirmations: 2 });
    });
  });
});
