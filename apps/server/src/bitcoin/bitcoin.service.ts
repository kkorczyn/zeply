import { Injectable } from "@nestjs/common";
import axios from "axios";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";

import { AddressResponse, TxResponse } from "./blockcypher.interface";
import { serializeAddressResponse, serializeTxResponse } from "./blockcypher.util";
import { TxInfo } from "./models/tx-info.model";
import { AddressInfo } from "./models/address-info.model";
import { AddressEntity } from "./entities/address.entity";

@Injectable()
export class BitcoinService {
  private readonly baseUrl = "https://api.blockcypher.com/v1/btc/main";

  constructor(@InjectRepository(AddressEntity) private addressRepo: Repository<AddressEntity>) {}

  async getAddressInfo(address: string): Promise<AddressInfo> {
    const url = `${this.baseUrl}/addrs/${address}`;

    try {
      this.incrementAddressSearch(address);
      const response = await axios.get<AddressResponse>(url);

      return serializeAddressResponse(response.data);
    } catch (error) {
      // Handle errors
      throw new Error("Failed to retrieve address information");
    }
  }

  async getTransactionInfo(txId: string): Promise<TxInfo> {
    const url = `${this.baseUrl}/txs/${txId}`;

    try {
      const response = await axios.get<TxResponse>(url);
      return serializeTxResponse(response.data);
    } catch (error) {
      // Handle errors
      throw new Error("Failed to retrieve transaction information");
    }
  }

  async incrementAddressSearch(address: string): Promise<AddressEntity> {
    try {
      let entity = await this.addressRepo.findOne({ where: { address } });
      if (!entity) {
        entity = this.addressRepo.create({ address, count: 1 });
      } else {
        entity.count++;
      }

      return this.addressRepo.save(entity);
    } catch (err) {
      // NOTE: I don't want to throw an error, becaue it will stop processing getAddressInfo
      console.log("Failed to increment address searches.");
    }
  }

  async getTopAddresses(): Promise<string[]> {
    const topAddresses = await this.addressRepo
      .createQueryBuilder("address")
      .orderBy("address.count", "DESC")
      .limit(5)
      .getMany();

    return topAddresses.map(({ address }) => address);
  }
}
