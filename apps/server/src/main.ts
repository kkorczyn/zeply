/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */
import { AppModule } from "./app/app.module";
import { Logger } from "@nestjs/common";
import { NestFactory } from "@nestjs/core";

async function bootstrap() {
  // NOTE: nx proxy is not working as expected, so I changed cors policy. Of course this is not a practice that I would follow on production app.
  const app = await NestFactory.create(AppModule, { cors: { origin: "*" } });
  const globalPrefix = "api";
  app.setGlobalPrefix(globalPrefix);
  const port = process.env.PORT || 3000;
  await app.listen(port);
  Logger.log(`🚀 Application is running on: http://localhost:${port}/${globalPrefix}`);
}

bootstrap();
