import { GraphQLModule } from "@nestjs/graphql";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Module } from "@nestjs/common";
import { ApolloDriver, ApolloDriverConfig } from "@nestjs/apollo";

import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { BitcoinModule } from "../bitcoin/bitcoin.module";
import { AddressEntity } from "../bitcoin/entities/address.entity";

@Module({
  imports: [
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: true,
    }),
    TypeOrmModule.forRoot({
      type: "sqlite",
      database: "addressDB",
      entities: [AddressEntity],
      synchronize: true,
    }),

    BitcoinModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
