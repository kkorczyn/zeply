# Zaply code challange

## Description:

The project is a TypeScript monorepo that consists of a simple application built using ReactJS (client) and a server built using NestJS. It interacts with the Bitcoin (BTC) blockchain using the [BlockCypher.com](https://www.blockcypher.com/) API to retrieve address and transaction information. The app allows users to subscribe to specific transaction hashes and receive notifications on the UI when changes occur. Additionally, users can choose the currency (USD, EUR, or BTC) in which the values should be displayed.

The monorepo structure is managed using Nx, which enables modular development and code sharing across different components and packages within the project.

The application also utilizes a **SQLite3** database, which is used to store and manage data locally. The database is created as a local file within the project.

## Table of Contents

- [Prerequisites](#prerequisites)
- [Getting Started](#getting-started)
- [Installation](#installation)
- [Usage](#usage)
- [Testing](#testing)
- [Folder Structure](#folder-structure)
- [License](#license)

## Prerequisites

Before you can use the software, please make sure you have the following prerequisites:

- BlockCypher.com API token
- CoinAPI.io API key

## Getting Started

### BlockCypher

To get started with this software, you will need to create an account on blockcypher.com and obtain an API token. The API token will be used to authenticate your requests to the BlockCypher API.

Please follow the steps below to create an account and obtain an API token:

1. Visit [blockcypher.com](https://www.blockcypher.com/) and click on the "Sign Up" button to create a new account.
2. Fill in the required information and complete the registration process.
3. Once you have successfully registered and logged in, navigate to the "Tokens" section of your account settings.
4. Create a new API token by clicking on the "Create New Token" button.

### CoinAPI

To get started with this software, you will need to obtain a CoinAPI API key. The API key will be used to authenticate your requests to the CoinAPI.

Please follow the steps below to obtain a CoinAPI API key:

1. Visit the [CoinAPI docs](https://docs.coinapi.io/) website.
2. In the top right corner of the website, fill in your email address in the provided field.
3. Click on the "GET A FREE API KEY" button.
4. A confirmation email containing your API key will be sent to the email address you provided.
5. Wait for a few minutes for the API key to activate.

## Installation

1. Clone the repository:

```shell
git clone git@gitlab.com:kkorczyn/zeply.git
```

2. Install the dependencies for the monorepo:

```shell
yarn install
```

3. Create a .env file by making a copy of the .example.env file.
4. Obtain a BlockCypher API token and CoinAPI API key by following the instructions in the "Getting Started" section of the README.
5. Open the newly created .env file in a text editor.
6. Replace the VITE_BLOCKCYPHER_TOKEN value with the API token you obtained from BlockCypher.
7. Replace the VITE_COINAPI_API_KEY value with the API token you obtained from CoinAPI.

Your .env file should now look like this:

```dotenv
VITE_BLOCKCYPHER_TOKEN=<your-blockcypher-token>
VITE_COINAPI_API_KEY=<your-coinapi-api-key>
VITE_GRAPHQL_ENDPOINT="http://localhost:3000/graphql"
```

## Usage

Start the development server and client app:

```shell
yarn start
```

Open the client app in your browser at http://localhost:4200.

## Testing

The project includes a comprehensive suite of tests to ensure the stability and correctness of the server. These tests cover various aspects of the application.

### Running Server Tests

To run the server tests, please follow the steps below:

1. Make sure you have installed the required dependencies by running `yarn install`.

2. Once the dependencies are installed, you can execute the following command to run the server tests:

```shell
yarn test:server:coverage
```

This command will run the server tests and generate a coverage report.

3. After the tests complete, a `coverage` folder will be created in the project's root directory. This folder contains the coverage results, including detailed information about the lines of code covered by the tests.

The coverage report can be accessed by opening the `index.html` file located within the `coverage` folder in a web browser. It provides valuable insights into the test coverage, highlighting areas that may require additional attention.

## Folder Structure

The project follows a monorepo structure using Nx. The repository contains the following directories:

```bash
├── apps
│   ├── client           # ReactJS client application
│   └── server           # NestJS server application
├── .env                 # Environment variables file
├── .example.env         # Example environment variables file
├── .gitignore
├── package.json
├── yarn.lock
└── README.md
```

## License

The project is licensed under the MIT License. Please refer to the LICENSE file for more details.
